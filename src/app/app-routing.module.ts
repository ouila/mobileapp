import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { NotLoggedGuardService } from "./services/not-logged-guard.service";
import { AuthGuardService } from "./services/auth-guard.service";

const routes: Routes = [
  { path: "", redirectTo: "menu", pathMatch: "full" },
  {
    path: "menu",
    canActivate: [NotLoggedGuardService],
    loadChildren: "./pages/public/menu/menu.module#MenuPageModule"
  },
  {
    path: "login",
    canActivate: [NotLoggedGuardService],
    loadChildren: "./pages/public/login/login.module#LoginPageModule"
  },
  {
    path: "register",
    canActivate: [NotLoggedGuardService],
    loadChildren: "./pages/public/register/register.module#RegisterPageModule"
  },

  {
    path: "member",
    canActivate: [AuthGuardService],
    loadChildren: "./pages/member/drawer-menu/drawer-menu.module#DrawerMenuPageModule"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
