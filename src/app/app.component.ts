import { Component } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { Uid } from "@ionic-native/uid/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { AuthenticationService } from "./services/authentication.service";
import { UserService } from "./services/user.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private uid: Uid,
    private androidPermissions: AndroidPermissions,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        this.statusBar.hide();
        this.splashScreen.hide();
        this.showStatusBar();
        this.getCameraPermission();
        this.getImei().then(value => {
          this.authenticationService.IMEI = value["imei"];
          this.authenticationService.MAC = value["mac"];
        });
      } else {
        this.authenticationService.IMEI = "0000001";
        this.authenticationService.MAC = "0000002";
      }
      this.authenticationService.authenticationState.subscribe(state => {
        if (state) {
          this.userService.checkUser().then(() => {
            this.router.navigate(["member"]);
          });
        } else {
          this.router.navigate(["menu"]);
        }
      });
    });
  }

  async getImei() {
    if (this.platform.is("android")) {
      const { hasPermission } = await this.androidPermissions.checkPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!hasPermission) {
        const result = await this.androidPermissions.requestPermission(
          this.androidPermissions.PERMISSION.READ_PHONE_STATE
        );

        if (!result.hasPermission) {
          throw new Error("Permissions required");
        }
        // ok, a user gave us permission, we can get him identifiers after restart app
        return;
      }
      return {
        imei: this.uid.IMEI,
        mac: this.uid.MAC
      };
    } else {
      //iOS
      return {
        imei: "0000003",
        mac: "0000004"
      };
    }
  }

  async getCameraPermission() {
    if (this.platform.is("android")) {
      const { hasPermission } = await this.androidPermissions.checkPermission(
        this.androidPermissions.PERMISSION.CAMERA
      );

      if (!hasPermission) {
        const result = await this.androidPermissions.requestPermission(
          this.androidPermissions.PERMISSION.CAMERA
        );

        if (!result.hasPermission) {
          throw new Error("Permission required");
        }
        return;
      }
    }
  }

  showStatusBar() {
    if (this.platform.is("android")) {
      this.statusBar.isVisible;
      this.statusBar.backgroundColorByHexString("#fff");
    }
  }
}
