import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

import { ReactiveFormsModule } from "@angular/forms";

import { IonicStorageModule } from '@ionic/storage';

import { BluetoothLE } from '@ionic-native/bluetooth-le/ngx';

import { HttpClientModule } from "@angular/common/http";
import { HTTP } from "@ionic-native/http/ngx";

//HTTP Provider
import { HttpProvider } from "./providers/http/http";
import { HttpNativeProvider } from "./providers/http-native/http-native";
import { HttpAngularProvider } from "./providers/http-angular/http-angular";

import { Uid } from '@ionic-native/uid/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { QrcodePageModule } from './pages/member/qrcode/qrcode.module';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from "src/environments/environment";
const config: SocketIoConfig = { url: 'http://192.168.0.12:6001', options: {}};
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config),
    AppRoutingModule,
    HttpClientModule,
    QrcodePageModule,
    ReactiveFormsModule
  ],
  providers: [
    HTTP,
    HttpProvider,
    HttpAngularProvider,
    HttpNativeProvider,
    Uid,
    AndroidPermissions,
    BarcodeScanner,
    BluetoothLE,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
