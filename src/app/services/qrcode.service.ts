import { Injectable } from '@angular/core';
import { HttpProvider } from "../providers/http/http";
import { environment } from "src/environments/environment";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  constructor(private http: HttpProvider) {
   }

   presence(code, token): Observable<any> {
     let body = {
       code: code,
       present: 1
     };
     let header = {
       Authorization: "Bearer " + token
     }
     return this.http.put(environment.basePath + "/api/presentQr",body, header);
   }

   show(token): Observable<any> {
     let header = {
       Authorization: "Bearer " + token
     }
     return this.http.get(environment.basePath + "/api/qrcode", {}, header);
   }
}
