import { Injectable } from "@angular/core";
import { HttpProvider } from "../providers/http/http";
import { BehaviorSubject, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Storage } from "@ionic/storage";
import { HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  TOKEN_KEY: string = "token";
  IMEI: string;
  MAC: string;
  API_TOKEN: string;
  TOKEN: string;
  authenticationState = new BehaviorSubject(false);
  constructor(private storage: Storage, private http: HttpProvider) {
    this.checkToken();
  }

  login(email: string, password: string): Observable<any> {
    let body = {
      email: email,
      password: password,
      imei: this.IMEI,
      mac: this.MAC
    };
    return this.http.post(environment.basePath + "/api/login", body, {});
  }

  logout() {
    return this.http.post(
      environment.basePath + "/api/logout",
      {},
      {
        Authorization: "Bearer " + this.TOKEN
      }
    );
  }

  register(
    lastname: string,
    firstname: string,
    email: string,
    password: string,
    confirm_password: string
  ): Observable<any> {
    let body = {
      lastname: lastname,
      firstname: firstname,
      email: email,
      password: password,
      confirm_password: confirm_password,
      admin: 0,
      imei: this.IMEI,
      mac: this.MAC
    };
    return this.http.post(environment.basePath + "/api/register", body);
  }
  removeToken() {
    this.storage.set(this.TOKEN_KEY, null).then(res => {
      this.authenticationState.next(false);
    });
    this.TOKEN = null;
  }

  setToken(token) {
    return this.storage.set(this.TOKEN_KEY, token).then(res => {
      if (res) {
        this.TOKEN = res;
        this.authenticationState.next(true);
      }
    });
  }

  checkToken() {
    return this.storage.get(this.TOKEN_KEY).then(res => {
      if (res) {
        this.TOKEN = res;
        this.authenticationState.next(true);
      }
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }
}
