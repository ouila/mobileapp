import { Injectable } from '@angular/core';
import { User } from 'src/helpers/user';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  USER_KEY: string = 'user';
  user: User;
  constructor(private storage: Storage) { 
    this.checkUser();
  }

  checkUser() {
    return this.storage.get(this.USER_KEY).then(res => {
      if (res) {
        this.user = res;
      }
    })
  }

  setUser(user: User) {
    this.storage.set(this.USER_KEY, user);
    this.user = user;
  }

}
