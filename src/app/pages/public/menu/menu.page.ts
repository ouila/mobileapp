import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  IMG_PATH: string = "assets/images/logo.jpg";
  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  goLogin(teacher){
    this.router.navigate(['login', {teacher: teacher}]);
  }
  goSubscribe(){
    this.router.navigate(['register']);
  }
}
