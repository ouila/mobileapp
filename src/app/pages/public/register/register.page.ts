import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/helpers/user';
import { AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  lastname: string;
  firstname: string;
  email: string;
  password: string;
  confirm_password: string;
  user: User;
  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService,
     private userService: UserService, private router: Router, private alertController: AlertController) {
    this.registerForm = this.formBuilder.group({
      lastname: new FormControl(null, [
        Validators.required
      ]),
      firstname: new FormControl(null, [
        Validators.required
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
            /**
       * MINIMUM: 
        * 8 characters
        * One uppercase
        * One lowercase
        * One number
        * One special character
       */ 
      password: new FormControl(null, [
        Validators.required,
        //Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ]),
      confirm_password: new FormControl(null, [
        Validators.required,
        //Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])
    }, {
      validator: this.mustMatch('password', 'confirm_password')
  });
   }
  ngOnInit() {
  }

  mustMatch(pwd: string, confirm_pwd: string){
    return (formGroup: FormGroup) => {
      const password = formGroup.controls[pwd];
      const confirm_password = formGroup.controls[confirm_pwd];

      if (confirm_password.errors && !confirm_password.errors.mustMatch) {
        return;
      }

      if (password.value != confirm_password.value) {
        confirm_password.setErrors({ mustMatch : true});
      } else {
        confirm_password.setErrors(null);
      }
    }
  }

  onSubmit(){
    let lastname = this.registerForm.get('lastname').value;
    let firstname = this.registerForm.get('firstname').value;
    let email = this.registerForm.get('email').value;
    let password = this.registerForm.get('password').value;
    let confirm_password = this.registerForm.get('confirm_password').value;
    this.authenticationService.register(lastname, firstname, email, password, confirm_password).subscribe(res => {
      if (res.error) {
        this.presentAlert('Erreur', res.error);
      } else {
        this.authenticationService.setToken(res.success.token);
        this.userService.setUser(new User(res.user));
        this.router.navigate(["member"]);
      }
    })
  }

  async presentAlert(title, errors) {
    let message = "";
    for (let key in errors) {
      message = message + "<br><br>" + "<b>" + errors[key][0] + "</b>";
    }
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
}
