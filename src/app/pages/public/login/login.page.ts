import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/helpers/user';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  user: User;
  loginForm: FormGroup;
  teacher: string;
  email: any;
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private authenticationService: AuthenticationService,
    private alertController: AlertController, private userService: UserService, private router: Router) {
    this.loginForm = this.formBuilder.group({
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      /**
       * MINIMUM: 
        * 8 characters
        * One uppercase
        * One lowercase
        * One number
        * One special character
       */ 
      password: new FormControl(null, [
        Validators.required,
        //Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])
    })
  }

  ngOnInit() {
    this.teacher = this.route.snapshot.paramMap.get("teacher");
  }

  onSubmit(){
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value
    this.authenticationService.login(email, password).subscribe(res => {
      if (res.error) {
        this.presentAlert('Erreur', res.error);
      } else {
        if (res.message){
          this.presentAlert('Warning', res.message);
        }
        this.authenticationService.setToken(res.success.token);
        this.userService.setUser(new User(res.user));
        this.router.navigate(["member"]);
      }
    })
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
