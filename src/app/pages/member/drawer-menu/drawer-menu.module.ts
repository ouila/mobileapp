import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DrawerMenuPage } from "./drawer-menu.page";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/member/home",
    pathMatch: "full"
  },
  {
    path: "",
    component: DrawerMenuPage,
    children: [
      {
        path: "home",
        loadChildren: "../home/home.module#HomePageModule"
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DrawerMenuPage]
})
export class DrawerMenuPageModule {}
