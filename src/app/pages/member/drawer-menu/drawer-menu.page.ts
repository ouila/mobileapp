import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/services/authentication.service";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import { BluetoothLE } from "@ionic-native/bluetooth-le/ngx";
import { QrcodeService } from "src/app/services/qrcode.service";
import { ModalController } from "@ionic/angular";
import { QrcodePage } from "../qrcode/qrcode.page";
import { Socket } from "ngx-socket-io";
import { Observable } from "rxjs";
@Component({
  selector: "app-drawer-menu",
  templateUrl: "./drawer-menu.page.html",
  styleUrls: ["./drawer-menu.page.scss"]
})
export class DrawerMenuPage implements OnInit {
  pages = [
    {
      title: "Home",
      url: "/member/home",
      icon: "home"
    }
  ];
  admin: Boolean = true;
  barcodeScannerOptions: BarcodeScannerOptions;
  //img_qrcode: String;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private userService: UserService,
    private barcodeScanner: BarcodeScanner,
    private bluetooth: BluetoothLE,
    private qrcodeService: QrcodeService,
    private modalController: ModalController
  ) {
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }

  ngOnInit() {
    if (this.userService.user.admin == 0) {
      this.admin = false;
    }
    /**Uncomment to use webosckets */
    // else {
    //   let that = this;
    //   this.socket.connect();
    //   this.socket
    //     .emit("subscribe", {
    //       channel: "laravel_database_test"
    //     })
    //     .on("App\\Events\\NewMessage", function(channel, data) {
    //       this.img_qrcode = data.message;
    //       if (this.img_qrcode != undefined) {
    //         that.showModal(this.img_qrcode);
    //       }
    //       console.log("img_qrcode",this.img_qrcode);
    //       console.log("Channel", channel);
    //       console.log("Data", data);
    //     });
    // }
  }

  /**
   * Scan QR Code
   */
  scanQRCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        this.qrcodeService
          .presence(barcodeData.text, this.authenticationService.TOKEN)
          .subscribe(res => {
            if (res.error) {
              console.error(res);
              alert("Erreur !");
            } else {
              alert("Vous avez bien été noté présent.");
            }
          });
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  /**
   * Get QR Code
   */
  showQRCode() {
    this.qrcodeService.show(this.authenticationService.TOKEN).subscribe(res => {
      this.showModal(res.qrcode);
    });
  }

  /**
   * Activate the Bluetooth
   */
  activateBluetooth() {
    this.bluetooth.initialize().subscribe(() => {
      this.bluetooth.enable();
      alert("Bluetooth activé");
    });
  }

  /**
   * Logout
   */
  logout() {
    this.authenticationService.logout().subscribe(res => {
      this.authenticationService.removeToken();
      this.router.navigate([""]);
    });
  }

  async showModal(img: String) {
    let modal = await this.modalController.create({
      component: QrcodePage,
      componentProps: {
        img: img
      }
    });

    await modal.present();
  }

  /**
   * SOCKET FUNCTIONS
   */
}
