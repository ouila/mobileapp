/**
 * Provider to manage http for native device or browser
 */
import { Injectable } from '@angular/core';

import { HttpAngularProvider } from '../http-angular/http-angular';
import { HttpNativeProvider } from '../http-native/http-native';
import { Platform } from '@ionic/angular';

@Injectable()
export class HttpProvider {
    private http: HttpNativeProvider | HttpAngularProvider;

    constructor(private platform: Platform, private angularHttp: HttpAngularProvider, private nativeHttp: HttpNativeProvider) {
        this.http = this.platform.is('cordova') ? this.nativeHttp : this.angularHttp;
    }

    public get(url: string, params?: any, options?: any) {
        return this.http.get(url, params, options);
    }

    public post(url: string, params?: any, options?: any) {
        return this.http.post(url, params, options);
    }

    public put(url: string, params?: any, options?: any) {
        return this.http.put(url, params, options);
    }

    public delete(url: string, params?: any, options?: any) {
        return this.http.delete(url, params, options);
    }
}