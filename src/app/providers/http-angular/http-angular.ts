/**
 * Provider used on browser when you run: ionic serve
 */
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class HttpAngularProvider {
  constructor(public http: HttpClient) { }

  public get(url: string, params?: any, options: any = {}) {
    if (options.Authorization) {
      return this.http.get(url, {
        headers: new HttpHeaders({
          'Authorization': options.Authorization
        })
      });
    } else {
      return this.http.get(url);
    }
  }

  public post(url: string, params: any, options: any = {}) {
  
    if (options.Authorization) {
      return this.http.post(url, params, {
        headers: new HttpHeaders({
          'Authorization': options.Authorization
        })
      });
    } else {
      return this.http.post(url, params, options);
    }

  }

  public put(url: string, params: any, options: any = {}) {
    let body = this.createSearchParams(params);
    if (options.Authorization) {
      return this.http.put(url, body.toString(), {
        headers: new HttpHeaders({
          'Authorization': options.Authorization
        })
      });
    } else {
      return this.http.put(url, body.toString(), options);
    }
  }

  public delete(url: string, params: any, options: any = {}) {
    return this.http.delete(url, options);
  }
  private createSearchParams(params: any) {
    let searchParams = new URLSearchParams();
    for (let k in params) {
      if (params.hasOwnProperty(k)) {
        searchParams.set(k, params[k]);
      }
    }
    return searchParams;
  }
}
