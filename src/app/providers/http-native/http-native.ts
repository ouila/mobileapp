/**
 * Provider used on device when you run: ionic cordova run android -l
 */
import { Injectable } from "@angular/core";
import { HTTP } from "@ionic-native/http/ngx";
import { from } from "rxjs";

@Injectable()
export class HttpNativeProvider {
  constructor(private http: HTTP) {}

  public get(url: string, params?: any, options: any = {}) {
    if (options.Authorization) {
      this.http.setHeader("*", "Authorization", options.Authorization);
    }
    this.http.setDataSerializer('json');
    let responseData = this.http
      .get(url, params, {})
      .then(resp =>
        options.responseType == "text" ? resp.data : JSON.parse(resp.data)
      );

    return from(responseData);
  }

  public post(url: string, params: any, options: any = {}) {
    if (options.Authorization) {
      this.http.setHeader("*", "Authorization", options.Authorization);
    }
    this.http.setDataSerializer('json');
    let responseData = this.http
      .post(url, params, options)
      .then(resp =>
        options.responseType == "text" ? resp.data : JSON.parse(resp.data)
      );
    return from(responseData);
  }

  public put(url: string, params: any, options: any = {}) {
    if (options.Authorization) {
      this.http.setHeader("*", "Authorization", options.Authorization);
    }
    this.http.setDataSerializer('json');
    let responseData = this.http
      .put(url, params, options)
      .then(resp =>
        options.responseType == "text" ? resp.data : JSON.parse(resp.data)
      );

    return from(responseData);
  }

  public delete(url: string, params: any, options: any = {}) {
    let responseData = this.http
      .delete(url, params, options)
      .then(resp =>
        options.responseType == "text" ? resp.data : JSON.parse(resp.data)
      );

    return from(responseData);
  }
}
