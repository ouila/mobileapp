export class User {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  admin: number;

  constructor(element: any){
    this.id = element.id;
    this.firstname = element.firstname;
    this.lastname = element.lastname;
    this.email = element.email;
    this.admin = element.admin;
  }
}